FROM debian:buster
MAINTAINER ksam <marvin@42.fr>

COPY srcs/ /

RUN apt update -y \
&& apt upgrade -y \
&& apt install -y vim \
&& apt install -y wget \
&& apt install -y curl \
&& apt install -y curl gnupg2 ca-certificates lsb-release \
&& echo "deb http://nginx.org/packages/debian `lsb_release -cs` nginx" \
    | tee /etc/apt/sources.list.d/nginx.list \
&& curl -fsSL https://nginx.org/keys/nginx_signing.key | apt-key add - \
&& apt update \
&& apt install nginx \
&& apt install -y php-fpm \
&& apt install -y php7.3-mysql \
&& apt install -y mariadb-server \
&& apt install -y openssl \
\
\
&& mv /mywebsite.conf /etc/nginx/conf.d \
&& rm /etc/nginx/conf.d/default.conf \
\
\
&& mkdir /var/www/ \
&& mkdir /var/www/mywebsite/ \
&& mkdir /etc/nginx/ssl \
&& openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
	-keyout /etc/nginx/ssl/nginx.key \
	-out /etc/nginx/ssl/nginx.crt \
    -subj "/C=FR/ST=FR/L=LYON/O=42/OU=Dev/CN=www.mywebsite.com" \
\
\
&& sed -i 's/^listen =.*$/listen = 127.0.0.1:9000/' /etc/php/7.3/fpm/pool.d/www.conf \
\
\
&& service nginx start \
&& service php7.3-fpm start \
&& service mysql start \
&& mysql -u root -e "CREATE DATABASE wordpress" \
&& mysql -u root -e "GRANT ALL ON wordpress.* TO user@localhost IDENTIFIED BY 'password'" \
\
\
&& cd /tmp/ \
&& wget https://files.phpmyadmin.net/phpMyAdmin/4.9.0.1/phpMyAdmin-4.9.0.1-all-languages.tar.gz \
&& tar -xvf phpMyAdmin-4.9.0.1-all-languages.tar.gz \
&& mv phpMyAdmin-4.9.0.1-all-languages/ /var/www/mywebsite/phpmyadmin \
\
\
&& wget -c https://wordpress.org/latest.tar.gz \
&& tar -xvzf latest.tar.gz \
&& mv wordpress/ /var/www/mywebsite/ \
&& chown -R www-data:www-data /var/www/mywebsite/wordpress/


CMD service php7.3-fpm start \
&& service mysql start \
&& service nginx start \
&& bash \
&& sh /int.sh